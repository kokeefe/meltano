---
metaTitle: Meltano Plugins, Extractors, and Loaders
description: Meltano Extractors and Loaders connect to external services to extract and load data for analysis.
sidebarDepth: 2
---

# Plugins

Meltano's ecosystem consist of plugins to help you facilitate your data pipeline. You can learn more about our extractors and loaders.

## Extractors

To learn more about extractors, see [our extractors documentation](/plugins/extractors/).

### Carbon Intensity

Extract raw data from Great Britain's Carbon Emissions API.

Detailed [documentation can be found here](/plugins/extractors/carbon-intensity.html).

### Comma Separated Values (CSV)

Extract raw data from any CSV.

Detailed [documentation can be found here](/plugins/extractors/csv.html).

### Facebook Ads

Extract Facebook Ads data from the Facebook Marketing API.

Detailed [documentation can be found here](/plugins/extractors/facebook.html).

### Fastly

Extract raw data from Fastly.

Detailed [documentation can be found here](/plugins/extractors/fastly.html).

### GitLab

Extract raw data from GitLab.

Detailed [documentation can be found here](/plugins/extractors/gitlab.html).

### Google Analytics

Extract raw data from Google Analytics.

Detailed [documentation can be found here](/plugins/extractors/google-analytics.html).

### Marketo

Extract raw data from Marketo.

Detailed [documentation can be found here](/plugins/extractors/marketo.html).

### MongoDB

Extract raw data from MongoDB.

Detailed [documentation can be found here](/plugins/extractors/mongodb.html).

### Salesforce

Extract raw data from Salesforce.

Detailed [documentation can be found here](/plugins/extractors/salesforce.html).

### Stripe

Extract raw data from Stripe.

Detailed [documentation can be found here](/plugins/extractors/stripe.html).

### Zendesk

Extract raw data from Zendesk.

Detailed [documentation can be found here](/plugins/extractors/zendesk.html).

## Loaders

To learn more about loaders, see [our loaders documentation](/plugins/loaders/).

### Comma Separated Values (CSV)

Load raw data into Comma Separated Values (CSV) for analysis.

Detailed [documentation can be found here](/plugins/loaders/csv.html).

### PostgreSQL

Load raw data into PostgreSQL for analysis.

Detailed [documentation can be found here](/plugins/loaders/postgres.html).

### Snowflake

Load raw data into Snowflake for analysis.

Detailed [documentation can be found here](/plugins/loaders/snowflake.html).

### SQLite

Load raw data into SQLite for analysis.

Detailed [documentation can be found here](/plugins/loaders/sqlite.html).
